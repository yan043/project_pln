<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@loginpost');

Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/', 'HomeController@index');

    // admin
    Route::get('/admin/pegawai', 'AdminController@masterPegawai');
    Route::post('/admin/pegawai/{id}', 'AdminController@save_pegawai');
    Route::get('/admin/pegawai/delete/{type}/{id}', 'AdminController@delete_pegawai');


    Route::get('/admin/unitInduk', 'AdminController@unitInduk');
    Route::post('/admin/unitInduk/{id}', 'AdminController@save_unit_induk');

    Route::get('/admin/unitPelaksana', 'AdminController@unitPelaksana');
    Route::post('/admin/unitPelaksana/{id}', 'AdminController@save_unit_pelaksanaan');

    Route::get('/admin/unitLayanan', 'AdminController@unitLayanan');
    Route::post('/admin/unitLayanan/{id}', 'AdminController@save_unit_layanan');

    Route::get('/admin/jabatan', 'AdminController@jabatan');
    Route::post('/admin/jabatan/{id}', 'AdminController@save_jabatan');

    Route::get('/admin/statusPegawai', 'AdminController@statusPegawai');
    Route::post('/admin/statusPegawai/{id}', 'AdminController@save_status_pegawai');

    Route::get('/admin/masterMobil', 'AdminController@masterMobil');
    Route::post('/admin/masterMobil/{id}', 'AdminController@save_mobil_unit');


    // transaksi
    Route::get('/step/{id_step}', 'OrderController@list');
    Route::get('/order/{id}', 'OrderController@listOrder');

    Route::get('/order/delete/{id}', 'OrderController@deleteOrder');

    Route::post('/saveInitOrder', 'OrderController@saveInitOrder');
    Route::post('/saveDispatchOrder', 'OrderController@saveDispatchOrder');
    Route::post('/saveIsiBBM', 'OrderController@saveIsiBBM');

    Route::get('/transaksi/monitoring', 'OrderController@monitoringTransaksi');

    Route::get('/review/{id}', 'ProgressController@review');

    Route::get('/logout', 'LoginController@logout');
});