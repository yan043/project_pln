<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>Let's and Go</title>

	<!-- Site favicon -->
	<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="/vendors/styles/style.css">
	@yield('css')
</head>
<body class="header-white sidebar-light">
	
			@include('desktop.partial.alerts')

			@yield('content')
	<script src="/vendors/scripts/core.js"></script>
	<script src="/vendors/scripts/script.min.js"></script>
	<script src="/vendors/scripts/process.js"></script>
	<script src="/vendors/scripts/layout-settings.js"></script>
	@yield('footer')
</body>
</html>