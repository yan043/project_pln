@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Master Pegawai')
@section('content')
<div class="card-box mb-30">
	<div class="pd-20">
		<h4 class="text-blue h4">
		<a href="#" data-toggle="modal" data-target="#Modal-TambahPegawai" type="button">
			<button class="btn btn-sm btn-primary">+</button>
        </a>&nbsp; Data
		</h4>
	</div>
	<div class="modal fade" id="Modal-TambahPegawai" role="dialog" aria-labelledby="myLargeModalLabelTambahPegawai" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabelTambahPegawai">Tambah Pegawai</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                    <form id="form_masterPegawai" method="post" action="/admin/pegawai/new">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">NIP</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan NIP" name="nip" type="text" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Nama</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan Nama" name="nama" type="text" required>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Password</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan Password" name="password" type="text" required>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Email</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan Email" name="email" type="email" required>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Unit Induk</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="unit_induk" style="width: 100%;" required>
									<option disabled>Pilih Unit Induk</option>
									@foreach ($unit_induk as $item)
									<option value="{{ $item->id }}">{{ $item->text }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Unit Pelaksana</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="unit_pelaksana" style="width: 100%;" required>
									<option disabled>Pilih Unit Pelaksana</option>
									@foreach ($unit_pelaksana as $item)
									<option value="{{ $item->id }}">{{ $item->text }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Unit Layanan</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="unit_layanan_pelanggan" style="width: 100%;" required>
									<option disabled>Pilih Unit Layanan</option>
									@foreach ($unit_layanan as $item)
									<option value="{{ $item->id }}">{{ $item->text }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Unit Mobil</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="unit_mobil" style="width: 100%;" required>
									<option disabled>Pilih Unit Mobil</option>
									@foreach ($unit_mobil as $item)
									<option value="{{ $item->id }}">{{ $item->text }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">NIP Atasan</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="nip_atasan" style="width: 100%;" required>
									<option disabled>Pilih NIP Atasan</option>
									@foreach ($nip_atasan as $item)
									<option value="{{ $item->id }}">{{ $item->text }} / {{ $item->nama }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Status Pegawai</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="id_status_pegawai" style="width: 100%;" required>
									<option disabled>Pilih Status Pegawai</option>
									@foreach ($status_pegawai as $item)
									<option value="{{ $item->id }}">{{ $item->text }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="pb-20 table-responsive">
		<table class="data-table table stripe hover">
			<thead>
				<tr>
					<th class="align-middle">Nama</th>
					<th class="align-middle">NIP</th>
                    <th class="align-middle">Email</th>
					<th class="align-middle">Unit Induk</th>
					<th class="align-middle">Unit Pelaksana</th>
					<th class="align-middle">Unit Layanan Pelanggan</th>
					<th class="align-middle">Unit Mobil</th>
					<th class="align-middle">Nomor Induk Atasan</th>
					<th class="align-middle">Status Pegawai</th>
					<th class="align-middle" class="datatable-nosort">Actions</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($data as $result)
				<tr>
					<td>{{ $result->nama }}</td>
					<td>{{ $result->nip }}</td>
					<td>{{ $result->email }}</td>
					<td>{{ $result->ui_nama }}</td>
                    <td>{{ $result->up_nama }}</td>
                    <td>{{ $result->nama_layanan }}</td>
                    <td>{{ $result->no_plat }}</td>
                    <td>{{ $result->nip_atasan }}</td>
                    <td>{{ $result->status_pegawai }}</td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">

								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#ModalEdit-{{ $result->id }}"><i class="dw dw-edit2"></i> Edit</a>
                                

								<a class="dropdown-item" href="/admin/pegawai/delete/pegawai/{{ $result->id }}"><i class="dw dw-delete-3"></i> Delete</a>
							</div>
						</div>
                        <div class="modal fade" id="ModalEdit-{{ $result->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabelEdit{{ $result->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myLargeModalLabelEdit{{ $result->id }}">Edit Pegawai - {{ $result->nip }}</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                        <form id="form_edit_{{ $result->id }}" method="post" action="/admin/pegawai/{{ $result->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">NIP</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan NIP" name="nip" type="text" value="{{ $result->nip }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Nama</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan Nama" name="nama" type="text" value="{{ $result->nama }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Password</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan Password" name="password" type="text" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Email</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan Email" name="email" type="email" value="{{ $result->email }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Unit Induk</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="unit_induk" style="width: 100%;" required>
                                                        <option value="" disabled>Pilih Unit Induk</option>
                                                        @foreach ($unit_induk as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->unit_induk) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Unit Pelaksana</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="unit_pelaksana" style="width: 100%;" required>
                                                        <option value="">Pilih Unit Pelaksana</option>
                                                        @foreach ($unit_pelaksana as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->unit_pelaksana) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Unit Layanan</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="unit_layanan_pelanggan" style="width: 100%;" required>
                                                        <option value="">Pilih Unit Layanan</option>
                                                        @foreach ($unit_layanan as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->unit_layanan) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Unit Mobil</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="unit_mobil" style="width: 100%;" required>
                                                        <option value="">Pilih Unit Mobil</option>
                                                        @foreach ($unit_mobil as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->unit_mobil) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">NIP Atasan</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="nip_atasan" style="width: 100%;" required>
                                                        <option value="">Pilih NIP Atasan</option>
                                                        @foreach ($nip_atasan as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->nip_atasan) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }} / {{ @$item->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Status Pegawai</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="id_status_pegawai" style="width: 100%;" required>
                                                        <option disabled>Pilih Status Pegawai</option>
                                                        @foreach ($status_pegawai as $item)
                                                        <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$result->id_status_pegawai) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</td>
				</tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
@endsection