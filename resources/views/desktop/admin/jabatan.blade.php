@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Jabatan')
@section('content')
<div class="card-box mb-30">
	<div class="pd-20">
		<h4 class="text-blue h4">
		<a href="#" data-toggle="modal" data-target="#Medium-modal" type="button">
            <button class="btn btn-sm btn-primary">+</button>
        </a>&nbsp; Data
		</h4>
	</div>
	<div class="modal fade" id="Medium-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Jabatan Pegawai</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                    <form id="form_jabatan" method="post" action="/admin/jabatan/new">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Nama Jabatan</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan Nama Jabatan" name="nama_jabatan" type="text">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="pb-20 table-responsive">
		<table class="data-table table stripe hover">
			<thead>
				<tr>
					<th>ID Jabatan</th>
                    <th>Nama Jabatan</th>
                    <th>Created At</th>
                    <th>Created By</th>
                    <th>Updated At</th>
                    <th>Updated By</th>
					<th class="datatable-nosort">Actions</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($data as $result)
				<tr>
					<td>{{ $result->id_jabatan }}</td>
                    <td>{{ $result->nama_jabatan }}</td>
                    <td>{{ $result->created_at }}</td>
                    <td>{{ $result->created_by }}</td>
                    <td>{{ $result->updated_at }}</td>
                    <td>{{ $result->updated_by }}</td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#ModalEdit-{{ $result->id_jabatan }}"><i class="dw dw-edit2"></i> Edit</a>
								<a class="dropdown-item" href="/admin/pegawai/delete/jabatan/{{ $result->id_jabatan }}"><i class="dw dw-delete-3"></i> Delete</a>
							</div>
						</div>

                        <div class="modal fade" id="ModalEdit-{{ $result->id_jabatan }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabelEdit{{ $result->id_jabatan }}" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myLargeModalLabelEdit{{ $result->id_jabatan }}">Tambah Jabatan Pegawai</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                        <form id="form_jabatan" method="post" action="/admin/jabatan/{{ $result->id_jabatan }}">
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Nama Jabatan</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan Nama Jabatan" name="nama_jabatan" value="{{ $result->nama_jabatan }}" type="text">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</td>
				</tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
@endsection