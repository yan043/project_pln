@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Unit Mobil')
@section('content')
<div class="card-box mb-30">
	<div class="pd-20">
		<h4 class="text-blue h4">
		<a href="#" data-toggle="modal" data-target="#Medium-modal" type="button">
            <button class="btn btn-sm btn-primary">+</button>
        </a>&nbsp; Data
		</h4>
	</div>
	<div class="modal fade" id="Medium-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Tambah Unit Mobil</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                    <form id="form_unitMobil" method="post" action="/admin/masterMobil/new">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">No Plat</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan No Plat" name="no_plat" type="text">
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Jenis Mobil</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="jenis" style="width: 100%;">
									<option disabled>Pilih Jenis Mobil</option>
									<option value="Mobil SUV">Mobil SUV</option>
                                    <option value="Mobil MPV">Mobil MPV</option>
                                    <option value="Mobil Crossover">Mobil Crossover</option>
                                    <option value="Mobil Hatchback">Mobil Hatchback</option>
                                    <option value="Mobil Sedan">Mobil Sedan</option>
                                    <option value="Mobil Sport Sedan">Mobil Sport Sedan</option>
                                    <option value="Mobil Convertible">Mobil Convertible</option>
                                    <option value="Mobil Station Wagon">Mobil Station Wagon</option>
                                    <option value="Mobil Off Road">Mobil Off Road</option>
                                    <option value="Mobil Pickup Truck & Mobil Double Cabin">Mobil Pickup Truck & Mobil Double Cabin</option>
                                    <option value="Mobil Elektrik">Mobil Elektrik</option>
                                    <option value="Mobil Hybrid">Mobil Hybrid</option>
                                    <option value="Mobil LCGC">Mobil LCGC</option>
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Nama Mobil</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Masukan Nama Mobil" name="nama_mobil" type="text">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="pb-20 table-responsive">
		<table class="data-table table stripe hover">
			<thead>
				<tr>
					<th>No Plat</th>
                    <th>Jenis</th>
                    <th>Nama Mobil</th>
                    <th>Created At</th>
                    <th>Created By</th>
                    <th>Updated At</th>
                    <th>Updated By</th>
					<th class="datatable-nosort">Actions</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($data as $result)
				<tr>
					<td>{{ $result->no_plat }}</td>
                    <td>{{ $result->jenis }}</td>
                    <td>{{ $result->nama_mobil }}</td>
                    <td>{{ $result->created_at }}</td>
                    <td>{{ $result->created_by }}</td>
                    <td>{{ $result->updated_at }}</td>
                    <td>{{ $result->updated_by }}</td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#ModalEdit-{{ $result->id }}"><i class="dw dw-edit2"></i> Edit</a>
								<a class="dropdown-item" href="/admin/pegawai/delete/masterMobil/{{ $result->id }}"><i class="dw dw-delete-3"></i> Delete</a>
							</div>
						</div>

                        <div class="modal fade" id="ModalEdit-{{ $result->id }}" role="dialog" aria-labelledby="myLargeModalLabelEdit{{ $result->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myLargeModalLabelEdit{{ $result->id }}">Tambah Unit Mobil</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                        <form id="form_unitMobil" method="post" action="/admin/masterMobil/{{ $result->id }}">
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">No Plat</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan No Plat" name="no_plat" type="text" value="{{ $result->no_plat }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Jenis Mobil</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <select class="custom-select2 form-control" name="jenis" style="width: 100%;">
                                                        <option value="" disabled>Pilih Jenis Mobil</option>
                                                        <option value="Mobil SUV">Mobil SUV</option>
                                                        <option value="Mobil MPV">Mobil MPV</option>
                                                        <option value="Mobil Crossover">Mobil Crossover</option>
                                                        <option value="Mobil Hatchback">Mobil Hatchback</option>
                                                        <option value="Mobil Sedan">Mobil Sedan</option>
                                                        <option value="Mobil Sport Sedan">Mobil Sport Sedan</option>
                                                        <option value="Mobil Convertible">Mobil Convertible</option>
                                                        <option value="Mobil Station Wagon">Mobil Station Wagon</option>
                                                        <option value="Mobil Off Road">Mobil Off Road</option>
                                                        <option value="Mobil Pickup Truck & Mobil Double Cabin">Mobil Pickup Truck & Mobil Double Cabin</option>
                                                        <option value="Mobil Elektrik">Mobil Elektrik</option>
                                                        <option value="Mobil Hybrid">Mobil Hybrid</option>
                                                        <option value="Mobil LCGC">Mobil LCGC</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-12 col-md-3 col-form-label">Nama Mobil</label>
                                                <div class="col-sm-12 col-md-9">
                                                    <input class="form-control" placeholder="Masukan Nama Mobil" name="nama_mobil" type="text" value="{{ $result->nama_mobil }}">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</td>
				</tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
@endsection