@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Transaksi Order')
@section('content')
<div class="row">
    <div class="col-md-6 col-sm-12 mb-30">
        <div class="pd-20 card-box height-100-p">
            <div class="clearfix mb-30">
                <div class="pull-left">
                    <h4 class="text-blue h4">Status Driver</h4>
                </div>
            </div>
            <div class="pb-20 table-responsive">
                <table id="table_status" class="data-table table stripe hover">
                    <thead>
                        <tr>
                            <th>NAMA</th>
                            <th>STATUS</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_driver as $value)
                    <tr>
                        <td>{{ $value->driver }}</td>
                        <td>
                            @if ($value->is_Onduty > $value->is_Standby)
                            STAND BY
                            @else
                            ON DUTY
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12 mb-30">
        <div class="pd-20 card-box height-100-p">
            <div class="clearfix mb-30">
                <div class="pull-left">
                    <h4 class="text-blue h4">Progress Driver</h4>
                </div>
            </div>
            <div class="pb-20 table-responsive">
                <table id="table_progress" class="data-table table stripe hover">
                    <thead>
                        <tr>
                            <th>NAMA</th>
                            <th>STATUS ON DUTY</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $progress = 0;
                    @endphp
                    @foreach ($data_driver as $value)
                        <tr>
                            <td>{{ $value->driver }}</td>
                            <td>
                                <div class="progress mb-20">
                                @php
                                    $progress = @round($value->is_Onduty / $value->is_Order * 100,2);
                                @endphp
								<div class="progress-bar bg-success" role="progressbar" style="width: {{ $progress }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">{{ $progress }}%</div>
							</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="card-box mb-30 col-md-12">
	<div class="pd-20">
    <h4 class="text-blue h4">
        <a href="#" data-toggle="modal" data-target="#Medium-modal" type="button">
            <button class="btn btn-sm btn-primary">+</button>
        </a>&nbsp; Order
    </h4>
	</div>
    <div class="modal fade" id="Medium-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Buat Order</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                    <form method="post" id="formorder" action="/saveInitOrder">
                        <input type="hidden" name="id" value="new">
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Tanggal Awal</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Pilih Tanggal Awal" name="start_date" type="date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Tanggal Akhir</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" placeholder="Pilih Tanggal Akhir" name="end_date" type="date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Asal</label>
                            <div class="col-sm-12 col-md-9">
                                <input class="form-control" name="asal" value="-3.007111, 114.386232" type="hidden">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1028.5056363805952!2d114.38568368807745!3d-3.007106560604159!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2de46efff0cedd05%3A0x72d7429c0ea9c7b1!2sPT%20PLN%20(Persero)%20UP3%20Kuala%20Kapuas!5e1!3m2!1sid!2sid!4v1645343482535!5m2!1sid!2sid" width="540" height="300" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Tujuan</label>
                            <div class="col-sm-12 col-md-9">
                                <select class="custom-select2 form-control" name="tujuan[]" multiple="multiple" style="width: 100%;">
                                    @foreach ($kota as $k)
                                    <option value="{{ $k->id }}">{{ $k->text }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-12 col-md-3 col-form-label">Kegiatan</label>
                            <div class="col-sm-12 col-md-9">
                                <textarea class="form-control" name="kegiatan"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" onclick="$('#formorder').submit()" class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="pb-20 table-responsive">
		<table id="table_order" class="data-table table stripe hover">
			<thead>
				<tr>
					<th>Nama Permintaan</th>
					<th>Tanggal Berangkat</th>
					<th>Tanggal Kembali</th>
                    <th>Jumlah Hari</th>
					<th>Tujuan</th>
					<th>Status</th>
					<th class="datatable-nosort">Actions</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($data as $result)
				<tr>
					<td>{{ $result->created_by }}</td>
					<td>{{ $result->start_date }}</td>
					<td>{{ $result->end_date }}</td>
                    @php
                        $tgl1 = new DateTime($result->start_date);
                        $tgl2 = new DateTime($result->end_date);
                        $jml_hari = $tgl2->diff($tgl1)->days + 1;
                    @endphp
					<td>{{ $jml_hari }} Hari</td>
                    <td>{{ $tujuankota[$result->id] }}</td>
                    <td>
                        @if($result->step_id == 1)
                        Pengajuan
                        @elseif(strcasecmp($result->step_id, '4') == 0)
                        Progress
                        @elseif($result->step_id == 5)
                        Penilaian Admin
                        @elseif($result->step_id == 6)
                        Completed
                        @endif
                    </td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                <a class="dropdown-item" href="/order/{{ $result->id }}" target="_blank"><i class="dw dw-paper-plane1"></i> Dispatch</a>
								<a class="dropdown-item" href="/order/delete/{{ $result->id }}"><i class="dw dw-delete-3"></i> Hapus</a>
							</div>
						</div>
					</td>
				</tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
<script>
    $('.table_status').DataTable({
        lengthChange: false,
        searching: false,
        paging: false,
        info: false,
        autoWidth: true
    });
</script>
@endsection