@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Monitoring Transaksi')
@section('content')
<div class="card-box mb-30">
	<div class="pd-20">
    <h4 class="text-blue h4">
        Monitoring
    </h4>
	</div>
	<div class="pb-20 table-responsive">
		<table class="data-table table stripe hover">
			<thead>
				<tr>
					<th>Tanggal Awal</th>
					<th>Tanggal Akhir</th>
                    <th>Jumlah Hari</th>
					<th>Asal</th>
					<th>Tujuan</th>
					<th>BBM Awal</th>
					<th>Driver</th>
					<th>Plat Mobil</th>
					<th>Kegiatan</th>
					<th class="datatable-nosort">Actions</th>
				</tr>
			</thead>
			<tbody>
                @foreach ($data as $result)
				<tr>
					<td>{{ $result->start_date }}</td>
					<td>{{ $result->end_date }}</td>
					@php
                        $tgl1 = new DateTime($result->start_date);
                        $tgl2 = new DateTime($result->end_date);
                        $jml_hari = $tgl2->diff($tgl1)->days + 1;
                    @endphp
					<td>{{ $jml_hari }} Hari</td>
					<td>{{ $result->asal }}</td>
                    <td>{{ $tujuankota[$result->id] }}</td>
					<td>Rp. {{ number_format($result->bbm_awal, 2, ",", ".") }}</td>
					<td>{{ $result->nama_driver }}</td>
					<td>{{ $result->no_plat }}</td>
                    <td>{{ $result->kegiatan }}</td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
								<a class="dropdown-item" href="/order/{{ $result->id }}"><i class="dw dw-edit2"></i> Edit</a>
								<a class="dropdown-item" href="/order/delete/{{ $result->id }}"><i class="dw dw-delete-3"></i> Hapus</a>
							</div>
						</div>
					</td>
				</tr>
                @endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- buttons for Export datatable -->
<script src="/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.print.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.html5.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.flash.min.js"></script>
<script src="/src/plugins/datatables/js/pdfmake.min.js"></script>
<script src="/src/plugins/datatables/js/vfs_fonts.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
@endsection