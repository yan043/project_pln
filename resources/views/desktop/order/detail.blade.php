@extends('desktop.layout')

@section('css')
@endsection
@section('title', 'Permintaan Order')
@section('content')
<div class="pd-20 card-box mb-30">
    <div class="clearfix">
        <div class="pull-left">
            <h4 class="text-blue h4">Permintaan</h4>
        </div>
    </div>
    <form method="POST" action="/saveDispatchOrder">
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Nama Pemohon</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{ session('auth')->nama }}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tujuan</label>
            <div class="col-sm-12 col-md-10">
                <textarea class="form-control" readonly>{{ $tujuankota[$data->id] }}</textarea>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tanggal Berangkat</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{ $data->start_date }}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Tanggal Kembali</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{ $data->end_date }}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Jumlah Hari</label>
            <div class="col-sm-12 col-md-10">
                @php
                    $tgl1 = new DateTime($data->start_date);
                    $tgl2 = new DateTime($data->end_date);
                    $jml_hari = $tgl2->diff($tgl1)->days + 1;
                @endphp
                <input class="form-control" type="text" value="{{ $jml_hari }}" readonly>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Perkiraan BBM Awal (Rupiah)</label>
            <div class="col-sm-12 col-md-10">
                <input class="form-control" name="bbm_awal" type="text" id="bbm_awal" placeholder="Perkiraan BBM Awal" value="{{ $data->bbm_awal ? : '0' }}" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Mobil</label>
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="col-sm-12 col-md-10">
                <select class="custom-select2 form-control" name="id_mobil" style="width: 100%;" required>
                    <option value="">-- Pilih Mobil --</option>
                    @foreach ($mobil as $item)
                    <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$data->id_mobil) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-12 col-md-2 col-form-label">Driver</label>
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="col-sm-12 col-md-10">
                <select class="custom-select2 form-control" name="id_driver" style="width: 100%;" required>
                    <option value="">-- Pilih Driver --</option>
                    @foreach ($driver as $item)
                    <option data-subtext="description 1" value="{{ $item->id }}" <?php if (@$item->id == @$data->id_driver) { echo "Selected"; } else { echo ""; } ?>>{{ @$item->text }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger">Reject</button>
            <button type="submit" class="btn btn-success">Approve</button>
        </div>
    </form>
</div>
@endsection
@section('footer')
<script type="text/javascript">
$(function() {
    $('#bbm_awal').val(function(index, value) {
		return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	});

	$('#bbm_awal').keyup(function(event) {
		if(event.which >= 37 && event.which <= 40) return;
		$(this).val(function(index, value) {
			return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		});
	});
});
</script>
@endsection