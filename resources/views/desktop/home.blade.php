@extends('desktop.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="/src/plugins/datatables/css/responsive.bootstrap4.min.css">
@endsection
@section('title', 'Halaman Utama')
@section('content')
<div class="card-box mb-30">
	<div class="pd-20">
		<h4 class="text-blue h4">Data Table Simple</h4>
		<p class="mb-0">you can find more options <a class="text-primary" href="https://datatables.net/" target="_blank">Click Here</a></p>
	</div>
	<div class="pb-20 table-responsive">
		<table class="data-table table stripe hover">
			<thead>
				<tr>
					<th class="table-plus datatable-nosort">Name</th>
					<th>Age</th>
					<th>Office</th>
					<th>Address</th>
					<th>Start Date</th>
					<th class="datatable-nosort">Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="table-plus">Gloria F. Mead</td>
					<td>25</td>
					<td>Sagittarius</td>
					<td>2829 Trainer Avenue Peoria, IL 61602 </td>
					<td>29-03-2018</td>
					<td>
						<div class="dropdown">
							<a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
								<i class="dw dw-more"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
								<a class="dropdown-item" href="#"><i class="dw dw-eye"></i> View</a>
								<a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Edit</a>
								<a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Delete</a>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('footer')
<script src="/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
<script src="/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
<!-- buttons for Export datatable -->
<script src="/src/plugins/datatables/js/dataTables.buttons.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.print.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.html5.min.js"></script>
<script src="/src/plugins/datatables/js/buttons.flash.min.js"></script>
<script src="/src/plugins/datatables/js/pdfmake.min.js"></script>
<script src="/src/plugins/datatables/js/vfs_fonts.js"></script>
<!-- Datatable Setting js -->
<script src="/vendors/scripts/datatable-setting.js"></script>
@endsection