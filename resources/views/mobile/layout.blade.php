<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<title>@yield('title')</title>
<link rel="stylesheet" type="text/css" href="/mobile/styles/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/mobile/fonts/bootstrap-icons.css">
<link rel="stylesheet" type="text/css" href="/mobile/styles/style.css">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@500;600;700&family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
<link rel="manifest" href="/_manifest.json">
<meta id="theme-check" name="theme-color" content="#FFFFFF">
<link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png">
@yield('css')
</head>

<body class="theme-light">

<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

<!-- Page Wrapper-->
<div id="page">
    <!-- Footer Bar -->
    <div id="footer-bar" class="footer-bar footer-bar-1 footer-bar-detached">
        <a href="/step/3"><i class="bi bi-bezier"></i><span>Work</span></a>
        <a href="#"><i class="bi bi-search"></i><span>Search</span></a>
        <a href="/" class="circle-nav-2 {{Request::path()=='home'?'active-nav':''}}"><i class="bi bi-house"></i><span>Home</span></a>
        <a href="#"><i class="bi bi-file-earmark-person"></i><span>Profile</span></a>
        <a href="/logout"><i class="bi bi-door-open"></i><span>Logout</span></a>
    </div>

    <!-- Page Content - Only Page Elements Here-->
    <div class="page-content footer-clear">
        <div class="pt-3">
            <div class="page-title d-flex">
                <div class="align-self-center me-auto">
                    <p class="color-highlight header-date"></p>
                    <h1>Selamat Datang, {{ session('auth')->nama }}</h1>
                </div>
                <div class="align-self-center ms-auto">

                </div>
            </div>
        </div>
        @yield('content')
    </div>
    @yield('menu')
    <div class="offcanvas offcanvas-bottom rounded-m offcanvas-detached" id="menu-install-pwa-android">
	   <div class="content">
		   <img src="/mobile/app/icons/icon-128x128.png" alt="img" width="80" class="rounded-m mx-auto my-4">
		   <h1 class="text-center">Install Let's Go APP</h1>
		   <p class="boxed-text-l">
			   Install Let's Go APP to your Home Screen to enjoy a unique and native experience.
		   </p>
		   <a href="#" class="pwa-install btn btn-m rounded-s text-uppercase font-900 gradient-highlight shadow-bg shadow-bg-s btn-full">Add to Home Screen</a><br>
		   <a href="#" data-bs-dismiss="offcanvas" class="pwa-dismiss close-menu color-theme text-uppercase font-900 opacity-60 font-11 text-center d-block mt-n1">Maybe later</a>
	   </div>
    </div>
    @if (Session::has('alertblock'))
    @php
        $type = 0;
        @if (session('alerts')['type']=='success')
            $type = 1;
        @endif
    @endphp
    <div id="menu-top-detached" class="offcanvas offcanvas-top rounded-m offcanvas-detached alert-auto-activate">
        <div class="d-flex m-3">
            <div class="align-self-center">
                <h2 class="font-700 mb-0">{{ session('alertblock')['type'] }}</h2>
            </div>
            <div class="align-self-center ms-auto">
                <a href="#" class="icon icon-xs me-n2" data-bs-dismiss="offcanvas">
                    <i class="bi bi-x-circle-fill color-{{ $type?'green':'red' }}-dark font-16"></i>
                </a>
            </div>
        </div>
        <div class="content mt-0">
            <h4 class="pt-0 mb-4">{{ session('alerts')['alertblock'] }}</h4>
            <a href="#" data-bs-dismiss="offcanvas" class="btn btn-full gradient-{{ $type?'green':'red' }} shadow-bg shadow-bg-xs">Oke!</a>
        </div>
    </div>
    @endif
</div>
@yield('footer')
<!-- End of Page ID-->

<script src="/mobile/scripts/bootstrap.min.js"></script>
<script src="/mobile/scripts/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@if (Session::has('alertblock'))
<script type="text/javascript">
    var autoActivates = new bootstrap.Offcanvas(document.getElementsByClassName('alert-auto-activate')[0])
    autoActivates.show();
</script>
@endif
@yield('js')
</body>