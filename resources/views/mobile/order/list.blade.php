@extends('mobile.layout')
@section('title', 'Work List')
@section('content')
<div class="content my-0 mt-n2 px-1">
    <div class="d-flex">
      <div class="align-self-center">
        <h3 class="font-16 mb-2">Work List</h3>
      </div>
    </div>
</div>
<div class="card card-style">
    <div class="content">
        <div class="tabs tabs-pill">
            <div class="collapse show">
                @forelse($data as $k => $d)
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-{{ $k }}">
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">{{ $d->kegiatan }}</h5>
                            <p class="mb-0 font-11 opacity-70">{{ $d->created_at }}</p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-green-dark">{{ $d->no_plat }}</h4>
                            <p class="mb-0 font-11"> {{ $d->nama_mobil }}</p>
                        </div>
                    </a>
                @empty
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@forelse($data as $k => $d)
<div id="menu-activity-{{ $k }}" class="offcanvas offcanvas-bottom offcanvas-detached rounded-m" style="display: block; visibility: hidden;" aria-hidden="true">
    <!-- menu-size will be the dimension of your menu. If you set it to smaller than your content it will scroll-->
    <div class="menu-size" style="height:385px;">
        <div class="content">
            <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-{{ $k }}">
                <div class="align-self-center ps-1">
                    <h5 class="pt-1 mb-n1">{{ $d->kegiatan }}</h5>
                    <p class="mb-0 font-11 opacity-70">{{ $d->created_at }}</p>
                </div>
                <div class="align-self-center ms-auto text-end">
                    <h4 class="pt-1 mb-n1 color-green-dark">{{ $d->no_plat }}</h4>
                    <p class="mb-0 font-11"> {{ $d->nama_mobil }}</p>
                </div>
            </a>
            <div class="row">
                <strong class="col-5 color-theme">Nama Pemohon</strong>
                <strong class="col-7 text-end">{{ $d->nama_pemohon }}</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Tujuan</strong>
                <strong class="col-7 text-end">{{ $tujuankota[$d->id] }}</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Tanggal Berangkat</strong>
                <strong class="col-7 text-end">{{ $d->start_date }}</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Tanggal Kembali</strong>
                <strong class="col-7 text-end">{{ $d->end_date }}</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <strong class="col-5 color-theme">Perkiraan BBM Awal</strong>
                <strong class="col-7 text-end color-highlight">Rp. {{ number_format($d->bbm_awal, 2, ",", ".") }}</strong>
                <div class="col-12 mt-2 mb-2"><div class="divider my-0"></div></div>
                <div class="col-12">
                    <a href="/order/{{ $d->id }}" class="btn btn-s btn-full gradient-green shadow-bg shadow-bg-xs">Mulai Perjalanan</a>
                </div>
            </div>
        </div>
    </div>
  </div>
@empty
@endforelse

@endsection

@section('js')
{{-- <script>
  $(function() {
  });
</script> --}}
@endsection