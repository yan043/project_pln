@extends('mobile.layout')
@section('title', 'Work List')
@section('content')
<div class="card card-style">
    <div class="content mb-0">
        <div class="tabs tabs-pill" id="tab-group-2">
            <div class="tab-controls rounded-m p-1 overflow-visible">
                <a class="font-13 rounded-s py-1 shadow-bg shadow-bg-s" data-bs-toggle="collapse" href="#tab-4" aria-expanded="true">Detail</a>
                <a class="font-13 rounded-s py-1 shadow-bg shadow-bg-s" data-bs-toggle="collapse" href="#tab-5" aria-expanded="false">Input BBM</a>
            </div>
            <div class="mt-3"></div>
            <!-- Detail -->
            <div class="collapse show" id="tab-4" data-bs-parent="#tab-group-2">
                <div class="form-custom form-label form-icon mt-3">
                    <i class="bi bi-pencil-fill font-12"></i>
                    <textarea class="form-control rounded-xs" readonly>{{ $tujuankota[$data->id] }}</textarea>
                    <label class="form-label-always-active color-highlight">Tujuan</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-calendar-plus-fill font-14"></i>
                    <input type="text" class="form-control rounded-xs" value="{{ $data->start_date }}" readonly/>
                    <label class="form-label-always-active color-highlight">Tanggal Berangkat</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-calendar-plus-fill font-14"></i>
                    <input type="text" class="form-control rounded-xs" value="{{ $data->end_date }}" readonly/>
                    <label class="form-label-always-active color-highlight">Tanggal Kembali</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-calendar-check-fill font-14"></i>
                    @php
                        $tgl1 = new DateTime($data->start_date);
                        $tgl2 = new DateTime($data->end_date);
                        $jml_hari = $tgl2->diff($tgl1)->days + 1;
                    @endphp
                    <input type="text" class="form-control rounded-xs" value="{{ $jml_hari }}" readonly/>
                    <label class="form-label-always-active color-highlight">Jumlah Hari</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-currency-dollar font-14"></i>
                    <input type="text" class="form-control rounded-xs" id="bbm_awal" value="{{ number_format($data->bbm_awal) ? : '0' }}" readonly/>
                    <label class="form-label-always-active color-highlight">Perkiraan BBM Awal</label>
                    <span>(Rupiah)</span>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-file-diff-fill font-14"></i>
                    <input type="text" class="form-control rounded-xs" id="liter_bbm_terisi" value="{{ number_format($total_liter_bbm) ? : '0' }}" readonly/>
                    <label class="form-label-always-active color-highlight">Liter BBM Sudah Diisi</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-currency-dollar font-14"></i>
                    <input type="text" class="form-control rounded-xs" id="rupiah_bbm_terisi" value="{{ number_format($total_rupiah_bbm) ? : '0' }}" readonly/>
                    <label class="form-label-always-active color-highlight">Rupiah BBM Sudah Terisi</label>
                    <span>(Rupiah)</span>
                </div>
            </div>
            
            <!-- Input BBM -->
            <form method="POST" action="/saveIsiBBM" enctype="multipart/form-data" autocomplete="off" id="input-bbm">
            <input type="hidden" name="master_id" value="{{ $data->id }}">
            <div class="collapse" id="tab-5" data-bs-parent="#tab-group-2">
                <div class="pt-3"></div>
                <div class="form-custom form-label form-icon mt-3">
                    <i class="bi bi-calendar-plus-fill font-14"></i>
                    <input type="date" class="form-control rounded-xs" name="tanggal_isi" required/>
                    <label class="form-label-always-active color-highlight">Tanggal Isi</label>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-currency-dollar font-14"></i>
                    <input type="number" class="form-control rounded-xs" id="harga" name="harga" required/>
                    <label class="form-label-always-active color-highlight">Rupiah Isi</label>
                    <span>(Rupiah)</span>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-file-diff-fill font-14"></i>
                    <input type="number" class="form-control rounded-xs" id="liter" name="liter" required/>
                    <label class="form-label-always-active color-highlight">BBM</label>
                    <span>(Liter)</span>
                </div>
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-file-diff-fill font-14"></i>
                    <input type="number" class="form-control rounded-xs" id="odo_meter" name="odo_meter" required/>
                    <label class="form-label-always-active color-highlight">ODO Meter</label>
                </div>
                @foreach ($photoInputs as $photo)
                <div class="pb-2"></div>
                <div class="form-custom form-label form-icon">
                    <i class="bi bi-file-image-fill font-14"></i>
                    <input type="file" accept="image/*" class="form-control rounded-xs" id="photo_{{ $photo }}" name="photo_{{ $photo }}" required/>
                    <label class="form-label-always-active color-highlight">Foto {{ str_replace('_', '', $photo) }}</label>
                </div>
                @endforeach
                <button class="btn btn-block gradient-highlight rounded-s shadow-bg shadow-bg-xs mt-3 mb-3" type="submit" style="text-align: center">Simpan</button>

                <hr>
                <div class="table-responsive">
                    <table class="table table-sm text-center">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Total Harga</th>
                                <th>Total Liter</th>
                                <th>ODO Meter</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($log_bbm as $bbm)
                            <tr>
                                <td>{{ $bbm->id }}</td>
                                <td>{{ number_format($bbm->harga) }}</td>
                                <td>{{ number_format($bbm->liter) }}</td>
                                <td>{{ number_format($bbm->odo_meter) }}</td>
                                <td>{{ $bbm->created_at }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(function() {
    $('#bbm_awal').val(function(index, value) {
		return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	});

	$('#bbm_awal').keyup(function(event) {
		if(event.which >= 37 && event.which <= 40) return;
		$(this).val(function(index, value) {
			return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		});
	});
});
</script>
@endsection