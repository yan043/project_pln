<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\DA\LoginModel;
use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rememberToken = $request->cookie('presistent-token');

        if ($rememberToken)
        {
            $user = LoginModel::getByToken($rememberToken);
            if ($user)
            {
                Session::put('auth', $user);
            }
        }
        Session::put('auth-originalUrl', $request->fullUrl());
        if (!Session::has('auth'))
        {
            if ($request->ajax())
            {
                return response('UNAUTHORIZED', 401);
            } else {
                return redirect('login');
            }
        }
        /*
        else{
            if(empty(session('auth')->witel) && !$request->is('profile/*')){
                return redirect('/profile');
            }
        }*/
        return $next($request);
    }
}