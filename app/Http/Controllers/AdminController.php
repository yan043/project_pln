<?php

namespace App\Http\Controllers;


use cURL;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent as Agent;
use App\DA\AdminModel;

date_default_timezone_set("Asia/Makassar");

class AdminController extends Controller
{

  protected $agentview;
  
  public function __construct()
  {
    $agent = new Agent();
    if ($agent->isMobile()) {
      $this->agentview = 'mobile';
    } else {
      $this->agentview = 'desktop';
    }
  }

  public function save_pegawai(Request $req, $id)
  {
    AdminModel::save_master_p($req, $id);

    return redirect('/admin/pegawai')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Data Pegawai!']
    ]);
  }

  public function save_unit_induk(Request $req, $id)
  {
    AdminModel::save_unit_induk($req, $id);

    return redirect('/admin/unitInduk')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Unit Induk!']
    ]);
  }

  public function save_unit_pelaksanaan(Request $req, $id)
  {
    AdminModel::save_unit_pelaksanaan($req, $id);

    return redirect('/admin/unitPelaksana')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Unit Pelaksana!']
    ]);
  }

  public function save_unit_layanan(Request $req, $id)
  {
    AdminModel::save_unit_layanan($req, $id);

    return redirect('/admin/unitLayanan')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Unit Layanan!']
    ]);
  }

  public function save_jabatan(Request $req, $id)
  {
    AdminModel::save_jabatan($req, $id);

    return redirect('/admin/jabatan')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Jabatan Pegawai!']
    ]);
  }

  public function save_status_pegawai(Request $req, $id)
  {
    AdminModel::save_status_pegawai($req, $id);

    return redirect('/admin/statusPegawai')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Status Pegawai!']
    ]);
  }

  public function save_mobil_unit(Request $req, $id)
  {
    AdminModel::save_mobil_unit($req, $id);

    return redirect('/admin/masterMobil')->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Simpan Unit Mobil!']
    ]);
  }

  public function masterPegawai()
  {
    $data = AdminModel::masterPegawai();
    $unit_induk = DB::table('unit_induk')->select('id_unit as id', 'nama_unit as text')->get();
    $unit_pelaksana = DB::table('unit_pelaksana')->select('id_pelaksana as id','nama_unit as text')->get();
    $unit_layanan = DB::table('unit_layanan')->select('id_layanan as id', 'nama_layanan as text')->get();
    $unit_mobil = DB::table('unit_mobil')->select('id', 'no_plat as text')->get();
    $nip_atasan = DB::table('master_pegawai')->select('nip as id', 'nip as text', 'nama')->get();
    $status_pegawai = DB::table('status_pegawai')->select('id_status as id', 'nama_status as text')->get();

    return view("$this->agentview.admin.masterPegawai", compact('data','unit_induk','unit_pelaksana','unit_layanan','unit_mobil','nip_atasan','status_pegawai'));
  }

  public function unitInduk()
  {
    $data = DB::table('unit_induk')->get();

    return view("$this->agentview.admin.unitInduk", compact('data'));
  }

  public function unitPelaksana()
  {
    $data = AdminModel::dataUnitPelaksana();
    $unit_induk = DB::table('unit_induk')->select('id_unit as id', 'nama_unit as text')->get();

    return view("$this->agentview.admin.unitPelaksana", compact('data','unit_induk'));
  }

  public function unitLayanan()
  {
    $data = AdminModel::dataUnitLayanan();
    $unit_induk = DB::table('unit_induk')->select('id_unit as id', 'nama_unit as text')->get();
    $unit_pelaksana = DB::table('unit_pelaksana')->select('id_pelaksana as id', 'nama_unit as text')->get();

    return view("$this->agentview.admin.unitLayanan", compact('data','unit_induk','unit_pelaksana'));
  }

  public function jabatan()
  {
    $data = DB::table('jabatan')->get();

    return view("$this->agentview.admin.jabatan", compact('data'));
  }

  public function statusPegawai()
  {
    $data = DB::table('status_pegawai')->get();

    return view("$this->agentview.admin.statusPegawai", compact('data'));
  }

  public function masterMobil()
  {
    $data = DB::table('unit_mobil')->get();

    return view("$this->agentview.admin.unitMobil", compact('data'));
  }

  public function master_delete($type, $id)
  {
    switch ($type) {
      case 'pegawai':
        DB::table('master_pegawai')->where('id', $id)->delete();
        break;
      
      case 'unitInduk':
        DB::table('unit_induk')->where('id_unit', $id)->delete();
        break;
      
      case 'unitPelaksana':
        DB::table('unit_pelaksana')->where('id_pelaksana', $id)->delete();
        break;
      
      case 'unitLayanan':
        DB::table('unit_layanan')->where('id_layanan', $id)->delete();
        break;
      
      case 'jabatan':
        DB::table('jabatan')->where('id_jabatan', $id)->delete();
        break;

      case 'statusPegawai':
        DB::table('status_pegawai')->where('id_status', $id)->delete();
        break;

      case 'masterMobil':
        DB::table('unit_mobil')->where('id', $id)->delete();
        break;
    }

    return redirect('/admin/'.$type)->with('alerts', [
      ['type' => 'success', 'text' => 'Berhasil Menghapus '.$type.'!']
    ]);
  }
}

?>