<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Jenssegers\Agent\Agent as Agent;
use App\DA\DashboardModel;

date_default_timezone_set("Asia/Makassar");

class DashboardController extends Controller
{
    protected $agentview;

    public function __construct()
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            $this->agentview = 'mobile';
        } else {
            $this->agentview = 'desktop';
        }
    }
}

?>