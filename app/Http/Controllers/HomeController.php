<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent as Agent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $agentview;

    public function __construct()
    {
        $this->middleware('auth');
        $agent = new Agent();
        if ($agent->isMobile()) {
            $this->agentview = 'mobile';
        } else {
            $this->agentview = 'desktop';
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("$this->agentview.home");
    }
}
