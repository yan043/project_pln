<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent as Agent;
use App\DA\OrderModel;

date_default_timezone_set("Asia/Makassar");

class OrderController extends Controller
{
    protected $agentview;

    public function __construct()
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            $this->agentview = 'mobile';
        } else {
            $this->agentview = 'desktop';
        }
    }

    public function list($id)
    {
        switch ($id) {
            case '1':
                $data = OrderModel::getByStep($id);
                $data_driver = OrderModel::dataDriver();
                foreach ($data as $value) {
                    $tujuan = '';
                    foreach(json_decode($value->tujuan) as $t)
                    {
                        $kota = DB::table('api_kota_portal')->where('id_kota', $t)->first();
                        $tujuan .= "( ".$kota->nama_kota." )";
                    }
                    $tujuankota[$value->id] = $tujuan;
                }
                $kota = OrderModel::kotajarak();
                $driver = OrderModel::get_driver();

                return view("$this->agentview.order.transaksi", compact('data', 'data_driver', 'kota', 'driver', 'tujuankota'));
                break;
            case '2':
                $data = OrderModel::getByStep($id);
                foreach ($data as $value) {
                    $tujuan = '';
                    foreach (json_decode($value->tujuan) as $t) {
                        $kota = DB::table('api_kota_portal')->where('id_kota', $t)->first();
                        $tujuan .= "( " . $kota->nama_kota . " )";
                    }
                    $tujuankota[$value->id] = $tujuan;
                }

                return view("$this->agentview.order.monitoring", compact('data', 'tujuankota'));
                break;
            case '3':
                $data = OrderModel::driver_order_list();
                foreach ($data as $value) {
                    $tujuan = '';
                    foreach (json_decode($value->tujuan) as $t) {
                        $kota = DB::table('api_kota_portal')->where('id_kota', $t)->first();
                        $tujuan .= "(".$kota->nama_kota . ")\n\n";
                    }
                    $tujuankota[$value->id] = $tujuan;
                }

                return view("$this->agentview.order.list", compact('data', 'tujuankota'));
                break;
        }
    }

    public function listOrder($id)
    {
        $data = OrderModel::getById($id);

        $tujuan = '';
        foreach (json_decode($data->tujuan) as $t)
        {
            $kota = DB::table('api_kota_portal')->where('id_kota', $t)->first();
            $check_jarak = OrderModel::hitungjarak(337, $t);
            $jarak_tempuh = json_decode($check_jarak)->jarak;

            $tujuan .= "- " . $kota->nama_kota . " (Jarak " . $jarak_tempuh . " KM)\n";
        }
        $tujuankota[$data->id] = $tujuan;

        $log_bbm = OrderModel::log_bbm($id);
        $total_liter_bbm = $total_rupiah_bbm = 0;
        foreach ($log_bbm as $key => $value)
        {
            $total_liter_bbm += $value->liter;
            $total_rupiah_bbm += $value->harga;
        }

        $photoInputs = ['ODO', 'Struk_BBM'];
        $mobil = OrderModel::get_unitMobil();
        $driver = OrderModel::get_driver();

        return view("$this->agentview.order.detail", compact('id', 'mobil', 'driver', 'data', 'tujuankota', 'total_liter_bbm', 'total_rupiah_bbm', 'photoInputs', 'log_bbm'));
    }

    public function saveInitOrder(Request $req)
    {
        $input = $req->only('start_date','end_date','asal','kegiatan');
        $input['tujuan'] = json_encode($req->tujuan);
        if ($req->id == 'new')
        {
            $input['step_id'] = 1;
            $input['created_by'] = session('auth')->nip;
            OrderModel::insert($input);
        } else {
            $input['step_id'] = 3;
            $input['modified_by'] = session('auth')->nip;
            OrderModel::update($req->id, $input);
        }
        return redirect('/step/1')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveDispatchOrder(Request $req)
    {
        $input = $req->all();
        $input['step_id'] = 2;
        $input['bbm_awal'] = str_replace('.', '', $req->input('bbm_awal'));
        $input['modified_by'] = session('auth')->nip;
        
        OrderModel::update($req->id,$input);
        return redirect('/step/1')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveTakeOrderDriver(Request $req)
    {
        $input = $req->all();
        $input['step_id'] = 4;
        OrderModel::update($req->id,$input);
        return redirect('/step/3')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveIsiBBM(Request $req)
    {
        $input['step_id'] = 4.1;
        OrderModel::updateBBM($req);
        return redirect('/order/' . $req['master_id'])->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveExtendHari(Request $req){

        $input = $req->all();
        $input['step_id'] = 4.2;
        OrderModel::update($req->id,$input);
        return redirect('/step/4')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveApprovalExtend(Request $req)
    {

        $input = $req->all();
        $input['step_id'] = 4.3;
        OrderModel::update($req->id,$input);
        return redirect('/step/4')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }
    public function saveProgressSelesai(Request $req)
    {

        $input = $req->all();
        $input['step_id'] = 5;
        OrderModel::update($req->id,$input);
        return redirect('/step/4')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }

    public function savePenilaian(Request $req)
    {
        $input = $req->all();
        $input['step_id'] = 6;
        OrderModel::update($req->id,$input);
        return redirect('/step/5')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Update Data!']
        ]);
    }

    public function deleteOrder($id)
    {
        OrderModel::delete($id);

        return redirect('/step/1')->with('alerts', [
            ['type' => 'success', 'text' => 'Berhasil Menghapus Data!']
        ]);
    }

}
?>