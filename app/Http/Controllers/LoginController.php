<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Jenssegers\Agent\Agent as Agent;
use App\DA\LoginModel;

date_default_timezone_set("Asia/Makassar");

class LoginController extends Controller
{
    protected $agentview;
    public function __construct()
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            $this->agentview = 'mobile';
        } else {
            $this->agentview = 'desktop';
        }
    }

    public function login()
    {
        return view("$this->agentview.auth.login");
    }

    public function loginpost(Request $req)
    {
        // dd($req->all(), $this->agentview);
        $data = LoginModel::login($req->nip, $req->password);
        if ($data)
        {
            $token = $data->token;

            if (!$data->token)
            {
                $token = LoginModel::setToken($data->id, md5($data->nip . microtime()));
            } else {
                // dd("token is already exist");
            }
            
            // dd($data, Session::has('auth-originalUrl'), Session::has('auth-originalUrl'));
            
            // if (Session::has('auth-originalUrl'))
            // {
            //     $url = Session::pull('auth-originalUrl');
            // } else {
            //     $url = '/';
            // }

            // dd($url);

            $response = redirect('/');

            // dd($response);

            if ($token)
            {
                $response->withCookie(cookie()->forever('presistent-token', $token));
            }
            return $response;
        } else {
            return redirect()->back()
            ->withInput($req->all())
            ->with('alertblock', ['type' => 'danger', 'text' => 'NIP atau Password Salah!']);
        }
    }
    public function logout()
    {
        Session::forget('auth');
        return redirect('/login')->withCookie(cookie()->forever('presistent-token', ''));
    }
}
?>