<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use cURL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent as Agent;
use App\DA\OrderModel;

date_default_timezone_set("Asia/Makassar");

class ProgressController extends Controller
{
    protected $agentview;

    public function __construct()
    {
        $agent = new Agent();
        if ($agent->isMobile()) {
            $this->agentview = 'mobile';
        } else {
            $this->agentview = 'desktop';
        }
    }

    public function review($id)
    {
        $data = OrderModel::getById($id);
        
        return view("$this->agentview.review.index", compact('id', 'data'));
    }
}
?>