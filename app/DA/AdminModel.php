<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Makassar");

class AdminModel
{
  public static function save_master_p($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('master_pegawai')->insert([
          'nip' => $req->nip,
          'unit_induk' => $req->unit_induk,
          'unit_pelaksana' => $req->unit_pelaksana,
          'unit_layanan_pelanggan' => $req->unit_layanan_pelanggan,
          'unit_mobil' => $req->unit_mobil,
          'nama' => $req->nama,
          'email' => $req->email,
          // 'id_jabatan' => $req->id_jabatan,
          'nip_atasan' => $req->nip_atasan,
          'id_status_pegawai' => $req->id_status_pegawai,
          'status_aplikasi' => $req->status_aplikasi,
          'password' => md5($req->password),
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('master_pegawai')->where('id', $id)->update([
          'nip' => $req->nip,
          'unit_induk' => $req->unit_induk,
          'unit_pelaksana' => $req->unit_pelaksana,
          'unit_layanan_pelanggan' => $req->unit_layanan_pelanggan,
          'unit_mobil' => $req->unit_mobil,
          'nama' => $req->nama,
          'email' => $req->email,
          // 'id_jabatan' => $req->id_jabatan,
          'nip_atasan' => $req->nip_atasan,
          'id_status_pegawai' => $req->id_status_pegawai,
          'status_aplikasi' => $req->status_aplikasi,
          'password' => md5($req->password),
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function save_unit_induk($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('unit_induk')->insert([
          'nama_unit' => $req->nama_unit,
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('unit_induk')->where('id_unit', $id)->update([
          'nama_unit' => $req->nama_unit,
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function save_unit_pelaksanaan($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('unit_pelaksana')->insert([
          'id_induk' => $req->id_induk,
          'nama_unit' => $req->nama_unit,
          'created_by' => session('auth')->nip,
        ]);
      }); 
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('unit_pelaksana')->where('id_pelaksana', $id)->update([
          'id_induk' => $req->id_induk,
          'nama_unit' => $req->nama_unit,
          'updated_by' => session('auth')->nip,
        ]);
      }); 
    }
  }

  public static function save_unit_layanan($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('unit_layanan')->insert([
          'id_induk' => $req->id_induk,
          'id_pelaksana' => $req->id_pelaksana,
          'nama_layanan' => $req->nama_layanan,
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('unit_layanan')->where('id_layanan', $id)->update([
          'id_induk' => $req->id_induk,
          'id_pelaksana' => $req->id_pelaksana,
          'nama_layanan' => $req->nama_layanan,
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function save_jabatan($req, $id)
  {
    if ($id == 'new') {
      return DB::transaction(function () use ($req) {
        DB::table('jabatan')->insert([
          'nama_jabatan' => $req->nama_jabatan,
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('jabatan')->where('id_jabatan', $id)->update([
          'nama_jabatan' => $req->nama_jabatan,
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function save_status_pegawai($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('status_pegawai')->insert([
          'nama_status' => $req->nama_status,
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('status_pegawai')->where('id_status', $id)->update([
          'nama_status' => $req->nama_status,
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function save_mobil_unit($req, $id)
  {
    if ($id == 'new')
    {
      return DB::transaction(function () use ($req) {
        DB::table('unit_mobil')->insert([
          'no_plat' => $req->no_plat,
          'jenis' => $req->jenis,
          'nama_mobil' => $req->nama_mobil,
          'created_by' => session('auth')->nip,
        ]);
      });
    } else {
      return DB::transaction(function () use ($req, $id) {
        DB::table('unit_mobil')->where('id', $id)->update([
          'no_plat' => $req->no_plat,
          'jenis' => $req->jenis,
          'nama_mobil' => $req->nama_mobil,
          'updated_by' => session('auth')->nip,
        ]);
      });
    }
  }

  public static function masterPegawai()
  {
      return DB::SELECT('
          SELECT
          mp.*,
          ui.nama_unit as ui_nama,
          up.nama_unit as up_nama,
          ul.nama_layanan,
          um.no_plat,
          sp.nama_status as status_pegawai
          FROM master_pegawai mp
          LEFT JOIN unit_induk ui ON mp.unit_induk = ui.id_unit
          LEFT JOIN unit_pelaksana up ON mp.unit_pelaksana = up.id_pelaksana
          LEFT JOIN unit_layanan ul ON mp.unit_layanan_pelanggan = ul.id_layanan
          LEFT JOIN unit_mobil um ON mp.unit_mobil = um.id
          LEFT JOIN status_pegawai sp ON mp.id_status_pegawai = sp.id_status
          GROUP BY mp.nip
      ');
  }

  public static function dataUnitPelaksana()
  {
    return DB::table('unit_pelaksana')
    ->leftJoin('unit_induk', 'unit_pelaksana.id_induk', '=', 'unit_induk.id_unit')
    ->select('unit_induk.nama_unit as unit_induk', 'unit_pelaksana.*')
    ->get();
  }

  public static function dataUnitLayanan()
  {
    return DB::SELECT('
      SELECT
      ui.nama_unit as unit_induk,
      up.nama_unit as unit_pelaksana,
      ul.*
      FROM unit_layanan ul
      LEFT JOIN unit_induk ui ON ul.id_induk = ui.id_unit
      LEFT JOIN unit_pelaksana up ON ul.id_pelaksana = up.id_pelaksana  
      ORDER BY ul.created_at DESC
    ');
  }
}

?>