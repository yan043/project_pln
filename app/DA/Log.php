<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class Log
{
    const TABLE = 'log';

    private static function joinTable()
    {
        return DB::table(self::TABLE)
            ->leftJoin('user', self::TABLE.'.user_id', '=', 'user.id')
            ->leftJoin('step', self::TABLE.'.step_id', '=', 'step.id')
            ->select('user.nip', 'user.nama', 'step',self::TABLE.'.*');
    }
    public static function getAll()
    {
       return self::joinTable()->get();
    }
    public static function getByStepId($id)
    {
        return self::joinTable()->where(self::TABLE.'.step_id', $id)->get();
    }
    public static function getByMasterId($id)
    {
        return self::joinTable()->where(self::TABLE.'.master_id', $id)->orderBy(self::TABLE.'.id','desc')->get();
    }
    public static function getByUserId($id)
    {
        return self::joinTable()->where(self::TABLE.'.user_id', $id)->get();
    }
    public static function getById($id)
    {
        return self::joinTable()->where('id', $id)->first();
    }
    public static function insert($input)
    {
        DB::table(self::TABLE)->insert($input);
    }
}