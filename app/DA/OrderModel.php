<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

date_default_timezone_set("Asia/Makassar");

class OrderModel
{
    const TABLE = 'master_order';
    
    private static function joinTable()
    {
        return DB::table(self::TABLE.' as mo')
        ->leftJoin('master_pegawai as mp', 'mo.id_driver', '=', 'mp.nip')
        ->leftJoin('master_pegawai as mpp', 'mo.modified_by', '=', 'mpp.nip')
        ->leftJoin('unit_mobil as um', 'mo.id_mobil', '=', 'um.id')
        ->leftJoin('step', 'mo.step_id', '=', 'step.id')
        ->select('mo.*', 'mp.nip as nip_driver', 'mp.nama as nama_driver', 'um.no_plat', 'um.jenis', 'um.nama_mobil', 'step.nama_step', 'mpp.nip as nip_pemohon', 'mpp.nama as nama_pemohon');
    }
    public static function monitoringTransaksi()
    {
        return self::joinTable()
        ->orderBy('mo.created_at','desc')
        ->get();
    }
    public static function getAll()
    {
        return self::joinTable()->get();
    }
    public static function getByStep($id)
    {
        return self::joinTable()->where('mo.step_id', $id)->get();
    }

    public static function getById($id)
    {
        return self::joinTable()->where('mo.id', $id)->first();
    }
    public static function insert($input)
    {
        return DB::table(self::TABLE)->insertGetId($input);
    }
    public static function update($id, $input)
    {
        DB::table(self::TABLE)->where('id', $id)->update($input);
    }
    public static function delete($id)
    {
        DB::table(self::TABLE)->where('id', $id)->delete();
    }
    public static function kota_kalimantan()
    {
        return DB::SELECT('SELECT akc.id_kecamatan, akc.nama as id, akc.nama as text FROM api_kecamatan akc LEFT JOIN api_kota ak ON akc.id_kota = ak.id_kota LEFT JOIN api_provinsi ap ON ak.id_provinsi = ap.id WHERE ap.id IN (61,62,63,64,65) ORDER BY akc.id_kecamatan');
    }
    public static function get_driver()
    {
        return DB::table('master_pegawai')->where('id_status_pegawai', 3)->select('nip as id', 'nama as text')->get();
    }
    public static function get_unitMobil()
    {
        return DB::table('unit_mobil')->select('id', 'no_plat as text')->get();
    }
    public static function driver_order_list()
    {
        return self::joinTable()->where('mp.nip', session('auth')->nip)->get();
    }
    public static function kotajarak()
    {
        return DB::table('api_kota_portal')->select('id_kota as id', 'nama_kota as text')->get();
    }
    public static function hitungjarak($id_asal, $id_tujuan)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apps.telkomakses.co.id/pradita/index.php?r=jaraktempuh/hitungjarak',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'idkotaasal=' . $id_asal . '&idkotatujuan=' . $id_tujuan . '',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: PHPSESSID=878a8bcb62201359015c2933674d10ef'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
    public static function log_bbm($id)
    {
        return DB::table('bbm')->where('master_id', $id)->get();
    }
    public static function updateBBM($req)
    {
        DB::table('master_order')->where('id', $req['master_id'])->update([
            'step_id' => $req['step_id']
        ]);

        $id_bbm = DB::table('bbm')->insertGetId($req->input());

        self::handleFileUpload($req, $id_bbm);
    }
    public static function handleFileUpload($req, $id_bbm)
    {
        $photoInputs = ['ODO', 'Struk_BBM'];
        foreach ($photoInputs as $name)
        {
            $input = 'photo_' . $name;
            $input_id = $id_bbm . '_photo_' . $name;

            $path = public_path() . "/upload/" . $req['master_id'] . "/";

            if (!file_exists($path))
            {
                if (!mkdir($path, 0770, true))
                {
                    return 'Gagal Menyiapkan Folder Mitra';
                }
            }

            if ($req->hasFile($input))
            {
                $file = $req->file($input);
                try {
                    $filename = $input_id . '.' . strtolower($file->getClientOriginalExtension());
                    $moved = $file->move("$path", "$filename");

                    $img = new \Imagick($moved->getRealPath());
                    $img->scaleImage(100, 150, true);
                    $img->writeImage("$path/$input_id-th.jpg");
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return 'Gagal Menyimpan File';
                }
            }

            print_r("success saved photos! $name\n");
        }
    }
    public static function dataDriver()
    {
        return DB::select('
            SELECT
                mp.nama as driver,
                SUM(CASE WHEN step.status_step = "STAND_BY" THEN 1 ELSE 0 END) as is_Standby,
                SUM(CASE WHEN step.status_step = "ON_DUTY" THEN 1 ELSE 0 END) as is_Onduty,
                COUNT(*) as is_Order
            FROM master_order mo
            LEFT JOIN master_pegawai mp ON mo.id_driver = mp.nip
            LEFT JOIN step ON mo.step_id = step.id
            WHERE
                mp.id_status_pegawai = 3
            GROUP BY driver
        ');
    }
}

?>