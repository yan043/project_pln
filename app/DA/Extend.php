<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;
class Extend
{
    const TABLE = 'extend';
    
    public static function getAll()
    {
        return DB::table(self::TABLE)->get();
    }
    public static function getById($id)
    {
        return DB::table(self::TABLE)->where('id', $id)->first();
    }
    public static function insert($input)
    {
        return DB::table(self::TABLE)->insertGetId($input);
    }
    public static function update($id, $input)
    {
        DB::table(self::TABLE)->where('id', $id)->update($input);
    }
    public static function delete($id)
    {
        DB::table(self::TABLE)->where('id', $id)->delete();
    }
}