<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

date_default_timezone_set("Asia/Makassar");

class LoginModel
{
  const TABLE = 'master_pegawai';

  public static function getAll()
  {
      return DB::table(self::TABLE)->get();
  }
  public static function getById($id)
  {
      return DB::table(self::TABLE)->where('id', $id)->first();
  }
  public static function getByToken($token)
  {
      return DB::table(self::TABLE)->where('token', $token)->first();
  }
  public static function login($nip,$pwd)
  {
      return DB::table(self::TABLE)->where('nip', $nip)->where('password', MD5($pwd))->first();
  }
  public static function insert($input)
  {
      return DB::table(self::TABLE)->insertGetId($input);
  }
  public static function update($id, $input)
  {
      DB::table(self::TABLE)->where('id', $id)->update($input);
  }
  public static function delete($id)
  {
      DB::table(self::TABLE)->where('id', $id)->delete();
  }
  public static function setToken($id, $token)
  {
      DB::table(self::TABLE)->where('id', $id)->update(["token" => $token]);
      return $token;
  }

}

?>